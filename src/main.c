/* Завдання під номером 25. Отримати корінь заданого числа, реалізуючи
алгоритм ітераційної формули Герона.
 */

#include <stdio.h>
//З ітераційної формули Герона константа лише одна - це точність розрахування кореня квадратного
#define ACCURACY 0.001

//Робимо попередню декларацію для функцій, використованих у цій программі
double module(double number);
double sqr(double number);
double sqrt_from_number(double given_number);

int main() {
  double sqrt_number, result_number;

  result_number = sqrt_from_number(sqrt_number);

  return 0;
}

//Функція для розрахування модуля переданого числа
double module(double number) {
  if(number < 0) {
    number = number * (-1);
  }

  return number;
}

//Функція для розрахування квадрата поданого числа
double sqr(double number) {
  number = number * number;

  return number;
}

//Функція для розрахування кореня квадратного поданого числа
double sqrt_from_number(double given_number) {
  /* Задаемо змінні, якими будемо користуватись:
    початкове значення числа,
    значення числа після ітераційної формули Герона,
    модуль різниці цих чисел.
    Змінні задані у відповідності до переліку */
  double x0, x1, module_of_difference;

  //Перед початком подальших розрахунків встановлюємо перші знечання для змінних
  x0 = given_number;
  //Для визначення x1 використовуємо ітераційну формулу Герона
  x1 = 0.5 * (x0 + given_number / x0);
  module_of_difference = module(x0 - x1);

  /* Для того, щоб розразувати корінь, нам потрібно повторяти розрахунок x1
    за допомогою формули Герона допоки модуль різниці x0 та x1 або квадрат цього модуля
    не був меншим за точність розрахування кореня квадратного помноженого на 2 */
  while(module_of_difference >= ACCURACY * 2 || sqr(module_of_difference) >= ACCURACY * 2) {
    x0 = x1;
    x1 = 0.5 * (x0 + given_number / x0);
    module_of_difference = module(x0 - x1);
  }

  //На виході з функції x0 буде дорівнювати нашому шуканому числу
  return x0;
}

